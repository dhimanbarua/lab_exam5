<?php
class MyCalculator{
    public $var1;
    public $var2;
    public function __construct($var1, $var2){
        $this->var1 = $var1;
        $this->var2 = $var2;
    }

    public function add(){
        return $this->var1+$this->var2;
    }
    public function subtract(){
        return $this->var1-$this->var2;
    }
    public function multiply(){
        return $this->var1*$this->var2;
    }
    public function divide(){
        return $this->var1/$this->var2;
    }
}
$Calculator = new MyCalculator(12, 2);
echo $Calculator->add()."<br >";
echo $Calculator->subtract()."<br >";
echo $Calculator->multiply()."<br >";
echo $Calculator->divide()."<br >";